app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('header',{
                    views:{
                        'header@':{
                            templateUrl: 'layout/header.html',
                            controller: 'HeaderCtrl'
                        }
                    },
                    parent:'footer'
                })
                .state('slider_header',{
                    views:{
                        'slider@':{
                            templateUrl: 'layout/slider.html',
                            controller: 'HeaderCtrl'
                        }
                    },
                    parent:'header'
                })
                .state('footer',{
                    views:{
                        'footer':{
                            templateUrl: 'layout/footer.html'
                        }
                    }
                })
                .state('home', {
                    url: '/home',
                    views:{
                        'container@':{
                            templateUrl: 'layout/home.html',
                            controller: 'HomeCtrl',

                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'slider_header' 
                })
                .state('contact', {
                    url: '/contact',
                    views:{
                        'container@':{
                            templateUrl: 'layout/contactus.html'
                            
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('mission', {
                    url: '/mission',
                    views:{
                        'container@':{
                            templateUrl: 'layout/mission.html'
                           
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('about', {
                    url: '/about',
                    views:{
                        'container@':{
                            templateUrl: 'layout/about.html'
                            
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('salient_features', {
                    url: '/salient_features',
                    views:{
                        'container@':{
                            templateUrl: 'layout/salient_features.html'
                            
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('admission', {
                    url: '/admission',
                    views:{
                        'container@':{
                            templateUrl: 'layout/admission.html'
                            
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                 .state('library', {
                    url: '/library',
                    views:{
                        'container@':{
                            templateUrl: 'layout/library.html'
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('seminar_hall', {
                    url: '/seminar_hall',
                    views:{
                        'container@':{
                            templateUrl: 'layout/seminar_hall.html'
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('health_care', {
                    url: '/health_care',
                    views:{
                        'container@':{
                            templateUrl: 'layout/health_care.html'
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                .state('internet', {
                    url: '/internet',
                    views:{
                        'container@':{
                            templateUrl: 'layout/internet.html'
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                 .state('e_library', {
                    url: '/e_library',
                    views:{
                        'container@':{
                            templateUrl: 'layout/e_library.html'
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
                  .state('cafeteria', {
                    url: '/cafeteria',
                    views:{
                        'container@':{
                            templateUrl: 'layout/cafeteria.html'
                        },
                        'leftbar@':{
                            templateUrl: 'layout/leftbar.html'
                            
                        },
                        'rightbar@':{
                            templateUrl: 'layout/rightbar.html'
                        }
                    },
                    parent:'header' 
                })
               .state('sports', {
                url: '/sports',
                views:{
                    'container@':{
                        templateUrl: 'layout/sports.html'
                    },
                    'leftbar@':{
                        templateUrl: 'layout/leftbar.html'
                        
                    },
                    'rightbar@':{
                        templateUrl: 'layout/rightbar.html'
                    }
                },
                parent:'header' 
            })
             .state('accommodation', {
            url: '/accommodation',
            views:{
                'container@':{
                    templateUrl: 'layout/accommodation.html'
                },
                'leftbar@':{
                    templateUrl: 'layout/leftbar.html'
                    
                },
                'rightbar@':{
                    templateUrl: 'layout/rightbar.html'
                }
            },
            parent:'header' 
        })
         .state('transportation', {
            url: '/transportation',
            views:{
                'container@':{
                    templateUrl: 'layout/transportation.html'
                },
                'leftbar@':{
                    templateUrl: 'layout/leftbar.html'
                    
                },
                'rightbar@':{
                    templateUrl: 'layout/rightbar.html'
                }
            },
            parent:'header' 
        })     
            $urlRouterProvider
                .when('/', '/home')
                .otherwise('home');
            return $stateProvider;
       }]);

